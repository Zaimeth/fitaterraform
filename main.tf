
resource "google_container_cluster" "cluster-fita" {
  name     = var.name
  location = var.single_zone

  network = "default" 
  subnetwork = "default"

  remove_default_node_pool = true
  initial_node_count       = 1

  release_channel {
    channel = "STABLE"
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "${google_container_cluster.cluster-fita.name}-node-pool"
  location   = var.single_zone
  cluster    = google_container_cluster.cluster-fita.name
  node_count = var.initial_node_count

  node_config {
    machine_type = var.machine_type
    disk_size_gb = var.disk_size_gb

    oauth_scopes = [ 
        "https://www.googleapis.com/auth/devstorage.read_only",
        "https://www.googleapis.com/auth/servicecontrol",
        "https://www.googleapis.com/auth/service.management.readonly",
        "https://www.googleapis.com/auth/trace.append"
    ]
  }
}
