terraform {
  backend "gcs" {
    credentials = "SA.json"
    bucket      = "terraform-state-ff"
    prefix      = "terraform/state"
  }
}